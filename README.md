# Audiophile

A user-unfriendly music player for audiophiles.

## Development ##

### Command Line argument parsing ###

We use [clap](https://github.com/clap-rs/clap "clap project at
GitHub.") v 3.0.0 for command line argument parsing.
