//use alsa::card;
use alsa::direct::pcm::MmapPlayback;
use alsa::pcm::Access;
use alsa::pcm::Format;
use alsa::pcm::HwParams;
use alsa::pcm::State;
use alsa::pcm::SwParams;
//use alsa::pcm::IO;
use alsa::pcm::PCM;
use alsa::Direction;
use alsa::ValueOr;

use std::fmt;

pub const FREQUENCY_PCM_44_1K: u32 = 44100;
pub const FREQUENCY_PCM_48K: u32 = 48000;
pub const FREQUENCY_PCM_88_2K: u32 = 88200;
pub const FREQUENCY_PCM_96K: u32 = 96000;
pub const FREQUENCY_PCM_176_4K: u32 = 176400;
pub const FREQUENCY_PCM_192K: u32 = 192000;
pub const FREQUENCY_PCM_352_8K: u32 = 352800;
pub const FREQUENCY_PCM_384K: u32 = 384000;
pub const FREQUENCY_PCM_705_6K: u32 = 705600;
pub const FREQUENCY_PCM_768K: u32 = 768000;

pub const FREQUENCY_DSD_64: u32 = 2822400;
pub const FREQUENCY_DSD_128: u32 = 5644800;
pub const FREQUENCY_DSD_256: u32 = 11289600;
pub const FREQUENCY_DSD_512: u32 = 22579200;

// TODO: Move this to the DSD crate?
fn pcm_frequency(dsd_frequency: u32) -> u32 {
    match dsd_frequency {
        FREQUENCY_DSD_64 => FREQUENCY_PCM_88_2K,
        FREQUENCY_DSD_128 => FREQUENCY_PCM_176_4K,
        FREQUENCY_DSD_256 => FREQUENCY_PCM_352_8K,
        FREQUENCY_DSD_512 => FREQUENCY_PCM_352_8K,
        _ => panic!(
            "Unrecognised DSD sampling frequency of {}Hz.",
            dsd_frequency
        ),
    }
}

pub enum AlsaSinkError {
    PCMErr,
    HwParamsError,
    MmapPlaybackError,
    SwParamsError,
}

pub struct AlsaSink {
    pcm: PCM,
}
impl AlsaSink {
    pub fn new(alsa_name: &str) -> Result<AlsaSink, AlsaSinkError> {
        let pcm = match PCM::new(alsa_name, Direction::Playback, false) {
            Ok(_pcm) => _pcm,
            Err(_) => return Err(AlsaSinkError::PCMErr),
        };

        Ok(AlsaSink { pcm })
    }

    pub fn init(
        &self,
        access: Access,
        channels: u32,
        format: Format,
        rate: u32,
    ) -> Result<(), AlsaSinkError> {
        let hwp = match HwParams::any(&self.pcm) {
            Ok(_hwp) => _hwp,
            Err(_) => return Err(AlsaSinkError::HwParamsError),
        };

        hwp.set_channels(channels).unwrap();
        hwp.set_rate(rate, ValueOr::Nearest).unwrap();
        hwp.set_format(format).unwrap();
        hwp.set_access(access).unwrap();
        self.pcm.hw_params(&hwp).unwrap();

        let swp = match self.pcm.sw_params_current() {
            Ok(_swp) => _swp,
            Err(_) => return Err(AlsaSinkError::SwParamsError),
        };

        swp.set_start_threshold(hwp.get_buffer_size().unwrap() - hwp.get_period_size().unwrap())
            .unwrap();
        self.pcm.sw_params(&swp).unwrap();

        Ok(())
    }

    pub fn set_samples_source<I: Iterator<Item = u32>>(
        &mut self,
        samples_iterator: &mut I,
    ) -> Result<i64, AlsaSinkError> {
        let mut mmpb = match self.pcm.direct_mmap_playback() {
            Ok(_mmpb) => _mmpb,
            Err(_) => return Err(AlsaSinkError::MmapPlaybackError),
        };

        Ok(mmpb.write(samples_iterator))
    }

    pub fn start(&self) -> Result<(), AlsaSinkError> {
        match self.pcm.start() {
            Ok(_) => Ok(()),
            Err(_) => Err(AlsaSinkError::MmapPlaybackError),
        }
    }

    pub fn state(&self) -> State {
        self.pcm.state()
    }

    pub fn print_state(&self) {
        match self.pcm.state() {
            State::Running => println!("Running"),
            State::Prepared => println!("Prepared"),
            State::XRun => println!("XRun"),
            State::Suspended => println!("Suspended"),
            State::Draining => println!("Draining"),
            _ => println!("Unexpected PCM state {:?}", self.pcm.state()),
        }
    }
}
impl fmt::Display for AlsaSink {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = String::from("AlsaSink:");

        if let Ok(info) = self.pcm.info() {
            s.push_str(&format!(
                "\nInfo:\nCard: {}\nDevice: {}\nSubdevice: {}\nId: {:?}\
                 \nName: {:?}\nSubdevice name: {:?}\n Stream: {:?}",
                info.get_card(),
                info.get_device(),
                info.get_subdevice(),
                info.get_id(),
                info.get_name(),
                info.get_subdevice_name(),
                info.get_stream()
            ));
        } else {
            s.push_str("\nFailed to get PCM info.");
        }

        if let Ok(hwp) = self.pcm.hw_params_current() {
            s.push_str(&format!(
                "\nHwParams:\nChannels: {}\nRate: {}\nFormat: {:?}\
                     \nAccess: {:?}\nPeriod size: {} frames\nBuffer size: {} frames ",
                hwp.get_channels().unwrap(),
                hwp.get_rate().unwrap(),
                hwp.get_format().unwrap(),
                hwp.get_access().unwrap(),
                hwp.get_period_size().unwrap(),
                hwp.get_buffer_size().unwrap()
            ));
        } else {
            s.push_str("\nFailed to get current HwParams struct.");
        }

        if let Ok(swp) = self.pcm.sw_params_current() {
            s.push_str(&format!(
                "\nSwParams:\nAvail Min: {} frames\nBoundary: {} frames\
                 \nStart threshold: {:?} frames\nStop threshold: {:?} frames\nTstamp mode: {:?}",
                swp.get_avail_min().unwrap(),
                swp.get_boundary().unwrap(),
                swp.get_start_threshold().unwrap(),
                swp.get_stop_threshold().unwrap(),
                swp.get_tstamp_mode().unwrap()
            ));
        } else {
            s.push_str("\nFailed to get current SwParams struct.");
        }

        f.write_str(&s)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
