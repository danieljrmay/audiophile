# Audiophile TODOs #

* [ ] Parse play single file command line argument.
* [ ] Parse a WAV file.
* [ ] Play a WAV file.
* [ ] Parse a DSF file.
* [ ] Play a DSF file.
* [ ] Parse a FLAC file.
* [ ] Play a FLAC file.
