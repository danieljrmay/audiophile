extern crate clap;
extern crate dsd;
#[macro_use]
extern crate log;
extern crate simplelog;

use clap::{App, Arg, ArgMatches};
use std::fmt;
use std::path::Path;

const DEFAULT_ALSA_DEVICE: &'static str = "iec958:CARD=D50,DEV=0";

fn main() {
    let config = Config::new();

    debug!("Starting up. Logging is working if you can see this!");

    match config.command() {
        Some("metadata") => exec_metadata(&config),
        Some("play") => exec_play(&config),
        _ => unreachable!("command has an impossible value."),
    }
}

struct Config {
    clargs: ArgMatches,
}
impl Config {
    fn new() -> Config {
        let clargs = App::new("Audiophile")
            .version("0.0.1")
            .author("Daniel J. R. May <daniel@danieljrmay.com>")
            .about("A user-unfriendly music player.")
            .arg(
                Arg::with_name("log-level")
                    .short('l')
                    .long("log-level")
                    .takes_value(true)
                    .possible_values(&["error", "warn", "info", "debug", "trace"])
                    .default_value("warn")
                    .help("Sets the logging level"),
            )
            .subcommand(
                App::new("metadata")
                    .about("Prints metadata of sound file(s)")
                    .arg(
                        Arg::with_name("paths")
                            .required(true)
                            .min_values(1)
                            .help("Filesystem path(s)"),
                    ),
            )
            .subcommand(
                App::new("play")
                    .about("Plays sound file(s)")
                    .arg(
                        Arg::with_name("output")
                            .short('o')
                            .long("output")
                            .takes_value(true)
                            .number_of_values(1)
                            .default_value(DEFAULT_ALSA_DEVICE)
                            .help("Set the output ALSA device"),
                    )
                    .arg(
                        Arg::with_name("paths")
                            .required(true)
                            .min_values(1)
                            .help("Filesystem path(s)"),
                    ),
            )
            .get_matches();

        let log_level = match clargs.value_of("log-level") {
            Some("error") => simplelog::LevelFilter::Error,
            Some("warn") => simplelog::LevelFilter::Warn,
            Some("info") => simplelog::LevelFilter::Info,
            Some("debug") => simplelog::LevelFilter::Debug,
            Some("trace") => simplelog::LevelFilter::Trace,
            _ => unreachable!("loglevel has an impossible value."),
        };

        simplelog::TermLogger::init(
            log_level,
            simplelog::Config::default(),
            simplelog::TerminalMode::Mixed,
        )
        .expect("Audiophile failed to configure simplelog::TermLogger.");

        Config { clargs }
    }

    fn command(&self) -> Option<&str> {
        self.clargs.subcommand_name()
    }

    fn music_path_list(&self) -> Option<Vec<MusicPath>> {
        let cmd_clargs = match self.clargs.subcommand() {
            ("metadata", Some(metadata_clargs)) => metadata_clargs,
            ("play", Some(play_clargs)) => play_clargs,
            _ => return None,
        };

        let paths_as_str_iter = match cmd_clargs.values_of("paths") {
            Some(iter) => iter,
            None => unreachable!("paths has an impossible value."),
        };

        let mut list: Vec<MusicPath> = Vec::new();

        for s in paths_as_str_iter {
            let p = Path::new(s);

            if p.exists() {
                debug!("Path '{}' exists.", s);

                if p.is_file() {
                    debug!("Path '{}' is a file.", s);

                    if let Some(ext_as_os_str) = p.extension() {
                        match ext_as_os_str.to_str() {
                            Some("dsf") => list.push(MusicPath::Dsf(p)),                                                            
                            Some("flac") => list.push(MusicPath::Flac(p)),
                            _ => warn!("Skipping path '{}' as it does not have a music file extension audiophile handles.", s),
                        }
                    } else {
                        warn!("Skipping path '{}' as it does not have an extension.", s);
                    }
                } else if p.is_dir() {
                    todo!("Handle directory paths.");
                // TODO: See Path.read_dir()
                } else {
                    warn!("Skipping path '{}' as it is not a file or directory.", s);
                }
            } else {
                warn!("Skipping path '{}' as it does not exist.", s);
            }
        }

        Some(list)
    }
}
impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&format!("command: {}", "TODO"))
    }
}

fn exec_metadata(config: &Config) {
    debug!("Executing metadata command.");

    if let Some(music_path_list) = config.music_path_list() {
        for music_path in music_path_list.iter() {
            match music_path {
                MusicPath::Dsf(dsf_path) => {
                    let dsf_path_as_str = match dsf_path.to_str() {
                        Some(p) => p,
                        None => panic!(),
                    };

                    match dsd::dsf::DSF::new_from_path(dsf_path) {
                        Ok(dsf) => println!("DSF metadata for {}:\n{}", dsf_path_as_str, dsf),
                        Err(err) => println!("ERROR in dsf metadata printing"),
                    }
                }
                MusicPath::Flac(flac_path) => {
                    metadata_flac(flac_path);
                }
            }
        }
    }
}

fn exec_play(config: &Config) {
    debug!("Executing play command.");

    if let Some(music_path_list) = config.music_path_list() {
        for music_path in music_path_list.iter() {
            match music_path {
                MusicPath::Dsf(dsf_path) => {
                    let dsf_path_as_str = match dsf_path.to_str() {
                        Some(p) => p,
                        None => panic!(),
                    };

                    match dsd::dsf::DSF::new_from_path(dsf_path) {
                        Ok(dsf) => play_dsf(dsf),
                        Err(err) => println!("ERROR in exec_play on dsf file"),
                    }
                }
                MusicPath::Flac(flac_path) => play_flac(flac_path),
            }
        }
    }
}

enum MusicPath<'a> {
    Dsf(&'a Path),
    Flac(&'a Path),
}

use alsa::direct::pcm::MmapPlayback;
use alsa::pcm::{Access, Format, HwParams, State, PCM};
use alsa::{Direction, ValueOr};
// use alsa_sink::AlsaSink;
// use alsa_sink::AlsaSinkError;
use dsd::dsf::DSF;

fn play_dsf(mut dsf: DSF) {
    println!("Play_dsf");

    // Open playback device
    let pcm = PCM::new(DEFAULT_ALSA_DEVICE, Direction::Playback, false).unwrap();

    // Set hardware parameters
    let hwp = HwParams::any(&pcm).unwrap();
    hwp.set_channels(dsf.metadata.fmt_chunk.channel_num)
        .unwrap();
    hwp.set_rate(
        pcm_frequency(dsf.metadata.fmt_chunk.sampling_frequency),
        ValueOr::Nearest,
    )
    .unwrap();
    hwp.set_format(Format::DSDU32BE).unwrap();
    hwp.set_access(Access::MMapInterleaved).unwrap();
    pcm.hw_params(&hwp).unwrap();

    // Make sure we don't start the stream too early
    let hwp = pcm.hw_params_current().unwrap();
    let swp = pcm.sw_params_current().unwrap();
    swp.set_start_threshold(hwp.get_buffer_size().unwrap() - hwp.get_period_size().unwrap())
        .unwrap();
    pcm.sw_params(&swp).unwrap();

    let mut mmpb: MmapPlayback<u32> = pcm.direct_mmap_playback().unwrap();

    let mut unfinished: bool = true;

    while unfinished {
        if mmpb.avail() > 0 {
            if mmpb.write(&mut dsf.sample_data) == 0 {
                pcm.drain().unwrap();
                unfinished = false;
            }
        }

        match mmpb.status().state() {
            State::Running => print!("\r{}", dsf.format_elapsed_of_total_duration()),
            State::Prepared => {
                println!("Prepared. Will now start output stream.");
                pcm.start().unwrap();
            }
            State::XRun => {
                println!("Underrun in audio output stream.");
                pcm.prepare().unwrap();
                unfinished = false;
            }
            State::Suspended => {
                println!("Resuming audio output stream.");
                pcm.resume().unwrap();
                unfinished = false;
            }
            State::Draining => {
                println!("PCM state is drainging and about to leave loop.");
                unfinished = false;
            }
            _ => {
                println!("Unexpected PCM state {:?}", mmpb.status().state());
                unfinished = false;
            }
        }
    }
}

pub const FREQUENCY_PCM_44_1K: u32 = 44100;
pub const FREQUENCY_PCM_48K: u32 = 48000;
pub const FREQUENCY_PCM_88_2K: u32 = 88200;
pub const FREQUENCY_PCM_96K: u32 = 96000;
pub const FREQUENCY_PCM_176_4K: u32 = 176400;
pub const FREQUENCY_PCM_192K: u32 = 192000;
pub const FREQUENCY_PCM_352_8K: u32 = 352800;
pub const FREQUENCY_PCM_384K: u32 = 384000;
pub const FREQUENCY_PCM_705_6K: u32 = 705600;
pub const FREQUENCY_PCM_768K: u32 = 768000;

pub const FREQUENCY_DSD_64: u32 = 2822400;
pub const FREQUENCY_DSD_128: u32 = 5644800;
pub const FREQUENCY_DSD_256: u32 = 11289600;
pub const FREQUENCY_DSD_512: u32 = 22579200;

fn pcm_frequency(dsd_frequency: u32) -> u32 {
    match dsd_frequency {
        FREQUENCY_DSD_64 => FREQUENCY_PCM_88_2K,
        FREQUENCY_DSD_128 => FREQUENCY_PCM_176_4K,
        FREQUENCY_DSD_256 => FREQUENCY_PCM_352_8K,
        FREQUENCY_DSD_512 => FREQUENCY_PCM_352_8K,
        _ => panic!(
            "Unrecognised DSD sampling frequency of {}Hz.",
            dsd_frequency
        ),
    }
}

fn metadata_flac(path: &Path) {
    println!("\nMetadata for {}", path.to_str().unwrap());

    let reader = claxon::FlacReader::open(path).unwrap();

    println!("\nStream info:");
    let stream_info = reader.streaminfo();
    println!("min_block_size: {}", stream_info.min_block_size);
    println!("max_block_size: {}", stream_info.max_block_size);

    match stream_info.min_frame_size {
        Some(mfs) => println!("min_frame_size: {}", mfs),
        None => println!("min_frame_size: None"),
    }

    match stream_info.max_frame_size {
        Some(mfs) => println!("max_frame_size: {}", mfs),
        None => println!("max_frame_size: None"),
    }

    println!("sample_rate: {}", stream_info.sample_rate);
    println!("channels: {}", stream_info.channels);
    println!("bits_per_sample: {}", stream_info.bits_per_sample);

    match stream_info.samples {
        Some(s) => println!("samples: {}", s),
        None => println!("samples: None"),
    }

    println!("md5sum: {:?}", stream_info.md5sum);

    println!("\nVorbis comments AKA FLAC tags:");
    for (name, value) in reader.tags() {
        println!("{}={}", name, value);
    }

    println!();
}

fn play_flac(path: &Path) {
    println!("Playing {}", path.to_str().unwrap());

    let mut flac_stream = FlacStream::new(path);

    // Open playback device
    let pcm = PCM::new(DEFAULT_ALSA_DEVICE, Direction::Playback, false).unwrap();

    // Set hardware parameters
    let hwp = HwParams::any(&pcm).unwrap();
    hwp.set_channels(flac_stream.channels()).unwrap();
    hwp.set_rate(flac_stream.rate(), ValueOr::Nearest).unwrap();
    hwp.set_format(Format::S32LE).unwrap();
    hwp.set_access(Access::MMapInterleaved).unwrap();
    pcm.hw_params(&hwp).unwrap();

    // Make sure we don't start the stream too early
    let hwp = pcm.hw_params_current().unwrap();
    let swp = pcm.sw_params_current().unwrap();
    swp.set_start_threshold(hwp.get_buffer_size().unwrap() - hwp.get_period_size().unwrap())
        .unwrap();
    pcm.sw_params(&swp).unwrap();

    let mut mmpb: MmapPlayback<i32> = pcm.direct_mmap_playback().unwrap();

    let mut unfinished: bool = true;

    debug!("About to start play loop.");

    while unfinished {
        if mmpb.avail() > 0 {
            if mmpb.write(&mut flac_stream) == 0 {
                pcm.drain().unwrap();
                unfinished = false;
            }
        }

        match mmpb.status().state() {
            State::Running => print!("\rRunning"),
            State::Prepared => {
                println!("Prepared. Will now start output stream.");
                pcm.start().unwrap();
            }
            State::XRun => {
                println!("Underrun in audio output stream.");
                pcm.prepare().unwrap();
                unfinished = false;
            }
            State::Suspended => {
                println!("Resuming audio output stream.");
                pcm.resume().unwrap();
                unfinished = false;
            }
            State::Draining => {
                println!("PCM state is drainging and about to leave loop.");
                unfinished = false;
            }
            _ => {
                println!("Unexpected PCM state {:?}", mmpb.status().state());
                unfinished = false;
            }
        }
    }
}

use claxon::input::BufferedReader;
use claxon::{FlacReader, FlacSamples};
use std::collections::VecDeque;
use std::path::PathBuf;

struct FlacStream {
    current_reader: FlacReader<std::fs::File>,
    buffer: VecDeque<i32>,
    channels: u32,
    rate: u32,
    paths: Vec<PathBuf>,
}
impl FlacStream {
    fn new(path: &Path) -> FlacStream {
        let path_buf = path.to_path_buf();
        let current_reader = FlacReader::open(&path_buf).expect("Failed to open FlacReader.");
        let stream_info = current_reader.streaminfo();
        let channels = stream_info.channels;
        let rate = stream_info.sample_rate;
        let paths = vec![path_buf];
        let buffer: VecDeque<i32> = VecDeque::new();

        let mut flac_stream = FlacStream {
            current_reader,
            buffer,
            channels,
            rate,
            paths,
        };

        flac_stream.fill_buffer();

        flac_stream
    }

    fn channels(&self) -> u32 {
        self.channels
    }

    fn rate(&self) -> u32 {
        self.rate
    }

    fn fill_buffer(&mut self) {
        debug!(
            "Starting to fill flac stream buffer, length is currently {}",
            self.buffer.len()
        );

        for result_sample in self.current_reader.samples() {
            match result_sample {
                Ok(s) => {
                    //debug!("Sample={}", s);
                    self.buffer.push_back(s.to_le())
                }
                Err(_) => panic!("Got an error when trying to fill flac stream buffer."),
            }
        }

        debug!(
            "Just filled flac stream buffer, length is now {}",
            self.buffer.len()
        );
    }
}
impl Iterator for FlacStream {
    type Item = i32;

    fn next(&mut self) -> Option<i32> {
        self.buffer.pop_front()
    }
}
